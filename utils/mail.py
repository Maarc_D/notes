#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
from email.Utils import COMMASPACE, formatdate
# Pour aller chercher le fichier en piece jointe
import os
import sys

frm = 'noreply@ssh.org'
to = ['your@maildomain.fr']
smtp_server = "smtps.server.fr"
smtp_port = 587
auth_user = 'your username on server'
auth_pwd = 'your password on server'

subject= "new ssh connection"
message = "default message"
files = ['path to pj 1', 'path to pj2', '...']
 
def send_mail(frm, to, subject, text, pj):
    print 'sending mail'
    message = MIMEMultipart()
    message['From'] = frm
    message['To'] = COMMASPACE.join(to)
    message['Date'] = formatdate(localtime=True)
    message['Subject'] = subject
    message.attach(MIMEText(text))
    
    for one_file in pj : 
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(one_file, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(one_file))
    
        message.attach(part)

    smtpserver = smtplib.SMTP(smtp_server, smtp_port)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo()
    smtpserver.login(auth_user, auth_pwd)
    
    smtpserver.sendmail(frm, to, message.as_string())
    print 'done!'
    smtpserver.close()

if (len(sys.argv) > 1) :
    message=sys.argv[1]
send_mail(frm, to, subject, message, files)
