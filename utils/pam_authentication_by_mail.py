#!/usr/bin/env python
import mail
import string
import random

def pam_sm_authenticate(pamh, flags, argv):

  try:
    user = pamh.get_user()
  except pamh.exception, e:
    return e.pam_result

    if user is None :
      msg = pamh.Message(pamh.PAM_ERROR_MSG, "Unable to send one time PIN.\nPlease contact your System Administrator")
      pamh.conversation(msg)
      return pamh.PAM_ABORT


  code=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
  mail.send_mail('noreply@ssh.org',['ur mail'], 'ssh authentication', code,[])
  prompt=pamh.Message(pamh.PAM_PROMPT_ECHO_ON, "Please enter the password received by email here : ")

  resp = pamh.conversation(prompt)
  
  if (resp.resp == code):
    return pamh.PAM_SUCCESS
  return pamh.PAM_AUTH_ERR

def pam_sm_setcred(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_acct_mgmt(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_open_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_close_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_chauthtok(pamh, flags, argv):
  return pamh.PAM_SUCCESS
