#!/usr/bin/python

#SERVEUR
from time import sleep
import socket
import BaseHTTPServer
from collections import deque
import select
import base64
import threading
import os

class handler(BaseHTTPServer.BaseHTTPRequestHandler):
	def do_HEAD(s):
		s.send_response(200)
		s.send_header("Content-type", "text/html")
		s.end_headers()
	def do_GET(s):
		handler.rondoudou(s)
	def do_POST(s):
		handler.rondoudou(s)
	def rondoudou(s):
		print "yeah !"
		data = ''
		if "=" in s.path:
			data = s.path[s.path.index('=')+1:]
			with open('/var/www/html/log', 'a') as myfile:
				myfile.write(base64.b64decode(data))
		s.send_response(200)
      		s.send_header("Content-Type", "text/html")
      		s.send_header("Content-Lenght", "0")
      		s.end_headers()


# initialisation des variables
http_server_info = ('', 55369)
http_server = BaseHTTPServer.HTTPServer(http_server_info, handler)
print 'Web server online'
http_server.serve_forever()