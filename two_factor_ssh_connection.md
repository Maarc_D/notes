# REFLEXION #

### HOW RUN A SCRIPT ON SSH CONNECTION ###

after some search on the internet i find that to run a script on ssh connection you must modify the **/etc/ssh/sshd_config**
and put : 

`ForceCommand /your/script/path`

###### CAUTION ######

*dont forget to allow the execution of your script* : `chmod +x /your/script/path`

*dont forget restart you ssh server : `/etc/init.d/ssh restart`*


### POC ###

first i do a pretty simple script : 
```sh
#!/bin/bash

echo "plz enter your recived password : ";
read mdp

if [ $mdp == "1234" ] ; then
	echo "you are now conected";
	$(/bin/bash)
else
	echo "checking failed";
	exit 42
fi
```
###### CAUTION ######

*dont forget restart you ssh server : `/etc/init.d/ssh restart`*

**IT WORKS**

### USE EMAIL AUTHENTICATION ###

using of send mail script writing in an old TP

[script here](utils/mail.py)

and modify prevfious bash script to generate a random password and send it by email using the python script (previous link).

```sh
#!/bin/bash

true_mdp=$(openssl rand -base64 6)
mail.py $true_mdp

echo "plz enter your recived password : ";
read mdp

if [ $mdp == $true_mdp ] ; then
	echo "you are now conected";
	/bin/bash
else
	echo "checking failed";
	exit 42
fi
```

###### bug ######

*scp is broken*

**I'll can probably fix it using pam**

## USING PAM ##

I found a module to use python script as pam module [here](http://pam-python.sourceforge.net/)

*note on fedora i'll must install python-devel and pam-devel on ubuntu : python-dev and libpam0g-dev*

```sh

tar -xvf pam-python-1.0.4.tar.gz
cd pam-python-1.0.4
make lib
# like me you may have a warning of XOPEN_SOURCE value changing in fedora you need add #undef_XOPEN_SOURCE after #undef	_POSIX_C_SOURCE in src/pam_python.c

sudo cp src/build/lib.linux-x86_64-2.7/pam_python.so /lib/security/ 

```
for the using of a python script as pam we need to define some functions

this is an exemple that allow everythings

```py

def pam_sm_authenticate(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_setcred(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_acct_mgmt(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_open_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_close_session(pamh, flags, argv):
  return pamh.PAM_SUCCESS

def pam_sm_chauthtok(pamh, flags, argv):
  return pamh.PAM_SUCCESS

```

to use your script as ssh authentication you must add the follow line in `/etc/pam.d/sshd` below the line `@include common-auth`:

`auth required pam_python.so /your/python/script.py` your script is suppose to be in `/lib/security`

mine is [here](utils/pam_authentication_by_mail.py)

edit `/etc/ssh/sshd_config` and set `ChallengeResponseAuthentication yes`

###### CAUTION ######
restart your ssh daemon
and your mail.py must be copy in `/usr/lib/python2.7/` to be import

with selinux don't forget : 

```
chcon -u system_u -r object_r -t ssh_home_t /lib/security/pam_python.so
restorecon -R /lib/security /lib/security/pam_python.so

chcon -u system_u -r object_r -t ssh_home_t /lib/security/ur_script.py
restorecon -R /lib/security /lib/security/ur_script.py

```

let's try.

### USE SMS AUTHENTICATION ###

#### SMS API ####

like [twilio](https://www.twilio.com/sms)

using an api the principle is exactly the same as the authentication by mail except we send the code by the api

#### GSM BOARD CONNECTED ON THE COMPUTER #####

i haven't enough material to try it, may be later.

# FINAL RESULTS #

use pam_python like I teach you before

# POSSIBLE IMPROVEMANT #

store the mail in passwd for each user with : 

`usermod $USER -c ',,your@mail.adress,'`

# KNOWN BUGS #